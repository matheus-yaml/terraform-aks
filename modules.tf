resource "azurerm_resource_group" "aks" {
    name    =   "aks"
    location    =   "East US 2"   
}

# module "network" {
#     source  =   "./modules/network"
#     resource_group  =  azurerm_resource_group.aks
# }

# module "vm" {
#     source          =   "./modules/vm"
#     resource_group  =   azurerm_resource_group.aks
#     vm_size         =   var.vm_size
#     subnets         =   module.network.subnets  
# }


# module "network_security_group" {
#     source  =   "./modules/network_security_group"
#     resource_group  =   azurerm_resource_group.aks
#     subnets         =   module.network.subnets

# }


module "aks" {
    source = "./modules/aks"

    resource_group  =   azurerm_resource_group.aks
    prefix          =   var.prefix
    vm_size         =   var.vm_size
}
