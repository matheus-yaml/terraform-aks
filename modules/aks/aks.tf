resource "azurerm_kubernetes_cluster" "aks" {
    name = "${var.prefix}"
    location = var.resource_group.location
    resource_group_name =   var.resource_group.name
    dns_prefix = "${var.prefix}"

    default_node_pool {
      name        = "${var.prefix}"
      node_count  = 2
      vm_size = var.vm_size
    }

    identity {
      type = "SystemAssigned"
    }

    tags = {
      Environment = "Production"
    }
}

