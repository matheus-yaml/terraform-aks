resource "azurerm_network_interface" "vm01" {
    name                =   "mv01"
    location            =   var.resource_group.location
    resource_group_name =   var.resource_group.name

    ip_configuration {
        name                            =   "ip-01"
        subnet_id                       =   var.subnets.id
        private_ip_address_allocation    =   "Dynamic"
        public_ip_address_id = azurerm_public_ip.ip01.id
    }

}
 
resource "azurerm_public_ip" "ip01" {
    name                =   "ip01"
    location            =   var.resource_group.location
    resource_group_name =   var.resource_group.name
    allocation_method   =   "Static"     
}

resource "azurerm_virtual_machine" "vm01" {
    name                    =   "vm01"
    location                =   var.resource_group.location
    resource_group_name     =   var.resource_group.name
    vm_size                 =   var.vm_size
    network_interface_ids   =   [azurerm_network_interface.vm01.id]


    storage_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "16.04-LTS"
        version   = "latest"
    }

    storage_os_disk {
        name    =   "disk01"
        create_option   =   "FromImage"
        managed_disk_type = "Standard_LRS"
    }

    os_profile {    
        computer_name  = "hostname"
        admin_username = "testadmin"
        admin_password = "Password1234!"
    }

    os_profile_linux_config {
        disable_password_authentication = false
    }
}
